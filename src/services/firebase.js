import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const config = {
    apiKey: "AIzaSyC1QFT5bzBnUpZmJapwP46VtFkjKj-hmpU",
    authDomain: "chatty-db277.firebaseapp.com",
    databaseURL: "https://chatty-db277.firebaseio.com",
    projectId: "chatty-db277",
    storageBucket: "chatty-db277.appspot.com",
    messagingSenderId: "913639615321",
    appId: "1:913639615321:web:a340409e951c2c8b58fa48",
    measurementId: "G-7ZWSP8FH33"
  };
  
  firebase.initializeApp(config);
  
  export const auth = firebase.auth;
  export const db = firebase.database();
